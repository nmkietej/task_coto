﻿using System;
using System.Collections.Generic;

namespace API_COTO.Models
{
    public partial class UserGroupItem
    {
        public int Id { get; set; }
        public int? UserId { get; set; }
        public int? ItemGroupId { get; set; }
        public string Notes { get; set; }
        public DateTimeOffset? CreatedDate { get; set; }
        public int? CreatedBy { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsDelete { get; set; }
        public int? SortOrder { get; set; }
    }
}
