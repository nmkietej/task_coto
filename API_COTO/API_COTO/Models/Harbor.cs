﻿using System;
using System.Collections.Generic;

namespace API_COTO.Models
{
    public partial class Harbor
    {
        public int Id { get; set; }
        public string HarborCode { get; set; }
        public string HarborName { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsDelete { get; set; }
        public string SearchString { get; set; }
        public int? SortOrder { get; set; }
        public int? CreatedBy { get; set; }
        public DateTimeOffset? CreatedDate { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTimeOffset? ModifiedDate { get; set; }
    }
}
