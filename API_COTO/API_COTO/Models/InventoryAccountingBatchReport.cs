﻿using System;
using System.Collections.Generic;

namespace API_COTO.Models
{
    public partial class InventoryAccountingBatchReport
    {
        public int InventoryAccountingBatchReportId { get; set; }
        public int InventoryAccountingPeriodId { get; set; }
        public int? ShipmentActualDetailId { get; set; }
        public string LotNumber { get; set; }
        public int? ProductId { get; set; }
        public DateTime? ProductionDateAccounting { get; set; }
        public DateTime? ExpiredDateAccounting { get; set; }
        public bool? IsImportFromAccounting { get; set; }
        public int? StoreIdAccounting { get; set; }
        public string NoteAccounting { get; set; }
        public double? BeginPeriodAccounting { get; set; }
        public double? ImportQuantityAccounting { get; set; }
        public double? ExportQuantityAccounting { get; set; }
        public double? EndPeriodAccounting { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsDelete { get; set; }
        public DateTimeOffset? CreatedDate { get; set; }
        public int? CreatedBy { get; set; }
        public DateTimeOffset? ModifyDate { get; set; }
        public int? ModifyBy { get; set; }
    }
}
