﻿using System;
using System.Collections.Generic;

namespace API_COTO.Models
{
    public partial class ProductListforErp
    {
        public int? Stt { get; set; }
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public string SuggestedCorrectName { get; set; }
        public string Column1 { get; set; }
        public string Ncc { get; set; }
        public string Column2 { get; set; }
        public string Origin { get; set; }
        public string TênThươngMạiSẽHiệnTrênWebsite { get; set; }
        public string DòngGiớiThiệuTómTắtVềSp { get; set; }
        public string NguồnGốcXuấtXứ { get; set; }
        public string CácQuyCáchĐóngGói { get; set; }
        public string DanhSáchTop5ỨngDụngĐiểnHìnhCủaMặtHàngNày { get; set; }
        public string WebsitesThamKhảoNếuCóVềMặtHàngNày { get; set; }
        public string ThuộcNhómHàng { get; set; }
    }
}
