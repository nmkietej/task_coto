﻿using System;
using System.Collections.Generic;

namespace API_COTO.Models
{
    public partial class SysConfig
    {
        public int Id { get; set; }
        public string ConfigCode { get; set; }
        public string ConfigValue { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsDelete { get; set; }
        public DateTimeOffset? CreatedDate { get; set; }
        public int? CreatedBy { get; set; }
        public DateTimeOffset? ModifyDate { get; set; }
        public int? ModifyBy { get; set; }
        public string SearchString { get; set; }
        public int? SortOrder { get; set; }
        public string Description { get; set; }
    }
}
