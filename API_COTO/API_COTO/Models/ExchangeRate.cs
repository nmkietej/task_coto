﻿using System;
using System.Collections.Generic;

namespace API_COTO.Models
{
    public partial class ExchangeRate
    {
        public int ExchangeRateId { get; set; }
        public string CurrencyId { get; set; }
        public decimal ExchangeRating { get; set; }
        public DateTimeOffset? ExchangeDate { get; set; }
        public string Notes { get; set; }
    }
}
