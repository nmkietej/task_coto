﻿using System;
using System.Collections.Generic;

namespace API_COTO.Models
{
    public partial class SupplierContractProduct
    {
        public int Id { get; set; }
        public int? SupplierContractid { get; set; }
        public int? ProductId { get; set; }
        public string ProductName { get; set; }
        public string OriginId { get; set; }
        public int? PackageUnitId { get; set; }
        public double? Quantity { get; set; }
        public double? UnitPrice { get; set; }
        public string Currency { get; set; }
        public double? ExpiredDate { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsDelete { get; set; }
        public DateTimeOffset? CreatedDate { get; set; }
        public int? CreatedBy { get; set; }
        public DateTimeOffset? ModifyDate { get; set; }
        public int? ModifyBy { get; set; }
    }
}
