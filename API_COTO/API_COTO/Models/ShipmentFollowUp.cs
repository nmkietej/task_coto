﻿using System;
using System.Collections.Generic;

namespace API_COTO.Models
{
    public partial class ShipmentFollowUp
    {
        public string ContractNo { get; set; }
        public string ContractSupplier { get; set; }
        public DateTime? Date { get; set; }
        public string IsSendInvoice { get; set; }
        public string Poankhai { get; set; }
        public DateTime? Etd { get; set; }
        public DateTime? Eta { get; set; }
        public string Supplier { get; set; }
        public string Customer { get; set; }
        public string Item { get; set; }
        public double? Quantity { get; set; }
        public double? UnitPrice { get; set; }
        public double? ValueUsd { get; set; }
        public string InvNo { get; set; }
        public DateTime? Invdate { get; set; }
        public string BLNo { get; set; }
        public double? InsFeePorttowarehouse { get; set; }
        public double? InsFeeDoortodoor { get; set; }
        public string IsSendAccount { get; set; }
        public string Note { get; set; }
    }
}
